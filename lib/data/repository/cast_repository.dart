import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:starmovies/data/config/api.dart';
import 'package:starmovies/data/model/cast.dart';

class CastAndCrewRepository {
  Future<List<CastModel>> getCastAndCrew(int movieid) async {
    final response = await http.get('${Config.baseUrl}/movie/${movieid}/credits?api_key=${Config.apiKey}');
    final cast = CastList.fromJson(json.decode(response.body)).cast;
    return cast;
  }
}