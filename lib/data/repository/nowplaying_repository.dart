import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:starmovies/data/config/api.dart';
import 'package:starmovies/data/model/movie_list.dart';

class NowPlayingMovieRepository {
  Future<List<MovieModel>> getNowPlayingMovies() async {
    final response = await http.get('${Config.baseUrl}/movie/now_playing?api_key=${Config.apiKey}');
    final movies = MovieList.fromJson(json.decode(response.body)).movieslist;
    return movies;
  }
}