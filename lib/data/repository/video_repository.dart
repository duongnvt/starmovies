import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:starmovies/data/config/api.dart';
import 'package:starmovies/data/model/movie_list.dart';
import 'package:starmovies/data/model/video.dart';

class VideoRepository {
  Future<List<VideoModel>> getVideo(int movieid) async {
    final response = await http.get('${Config.baseUrl}/movie/${movieid}/videos?api_key=${Config.apiKey}');
    final videos = VideoList.fromJson(json.decode(response.body)).videolist;
    return videos;
  }
}