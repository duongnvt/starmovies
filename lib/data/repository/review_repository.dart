import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:starmovies/data/config/api.dart';
import 'package:starmovies/data/model/review.dart';

class ReviewRepository {
  Future<List<ReviewModel>> getReview(int movieid) async {
    final response = await http.get('${Config.baseUrl}/movie/${movieid}/reviews?api_key=${Config.apiKey}');
    final reviews = ReviewList.fromJson(json.decode(response.body)).review;
    return reviews;
  }
}