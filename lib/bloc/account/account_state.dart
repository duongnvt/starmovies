part of 'account_cubit.dart';

class AccountState extends Equatable {
  bool logined;

  AccountState({this.logined = false});
  AccountState copyWith({bool logined}) {
    return AccountState(logined: logined ?? this.logined);
  }

  @override
  List<Object> get props => [this.logined];
}

// class AccountInitial extends AccountState {
//   AccountInitial(bool logined) : super(logined);
// }
