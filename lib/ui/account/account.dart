import 'package:flutter/material.dart';
import 'package:starmovies/bloc/account/account_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:starmovies/ui/account/profile_Logined.dart';
import 'package:starmovies/ui/account/profile_not_login.dart';

class AccountPage extends StatefulWidget {
  AccountPage({Key key}) : super(key: key);

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  AccountCubit _cubit;
  @override
  void initState() {
    _cubit = context.read<AccountCubit>();
    super.initState();
  }

  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      child: BlocBuilder<AccountCubit, AccountState>(
        builder: (context, state) {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Color(0xFF0F1B2B),
            child: state.logined ? ProfileLogined() : ProfileNotLogin(),
          );
        },
      ),
    )));
  }
}
