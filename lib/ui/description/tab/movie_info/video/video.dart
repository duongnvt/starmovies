import 'package:flutter/material.dart';
import 'package:starmovies/data/model/video.dart';
import 'package:starmovies/ui/app_bar/app_bar.dart';
import 'package:starmovies/ui/description/tab/movie_info/video/item_video.dart';


class Video extends StatefulWidget {
  final List<VideoModel> listvideo;
  const Video({
    Key key,
    @required this.listvideo,
  }) : super(key: key);
  @override
  State<Video> createState() => _VideoState();
}

class _VideoState extends State<Video> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Color(0xFF0F1B2B),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              AppBarMovie(
                title: 'Videos',
                forward: false,
              ),
              Expanded(
                  child: Container(
                    child: ItemVideo(listvideo: widget.listvideo,),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
